package com.users;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.users.models.User;

public class UserActivity extends AppCompatActivity {

    private User user;
    private TextView textViewUser;
    private TextView textViewType;
    private ImageView imageViewAvatar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Bundle bundle = this.getIntent().getExtras();
        if (bundle != null) {
            user = bundle.getParcelable("user");
            textViewUser = (TextView)findViewById(R.id.textViewUser);
            textViewUser.setText(user.getLogin());

            textViewType = (TextView)findViewById(R.id.textViewType);
            textViewType.setText(user.getType());

            imageViewAvatar = (ImageView)findViewById(R.id.imageUser);
            Picasso.with(this).load(user.getAvatarUrl()).into(imageViewAvatar);
        }

    }

}
