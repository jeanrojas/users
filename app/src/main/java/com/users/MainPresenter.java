package com.users;

import android.content.Context;
import android.widget.ListView;

import com.users.adapters.UserAdapter;
import com.users.models.User;
import com.users.webservice.RestClient;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by JEAN CARLO on 26/2/2016.
 */
public class MainPresenter implements Callback<List<User>>{

    private Context mContext;
    private List<User> users;
    private ListView listViewUsers;
    private int mCount;
    private ArrayList<User> reverseUsers;

    public void setView(Context context, ListView listView, int count){
        listViewUsers = listView;
        mContext = context;
        mCount = count;
    }

    public void getData(){
        Call<List<User>> call = RestClient.getRestClientInstance().getService().getUsers();
        call.enqueue(this);
    }

    @Override
    public void onResponse(Call<List<User>> call, Response<List<User>> response) {
        users = response.body();
        if (users.size() > 0){
            ArrayList<User> usersArray = new ArrayList<>(users.size());
            usersArray.addAll(users);
            UserAdapter userAdapter = new UserAdapter(mContext, getLimitItems(usersArray));
            listViewUsers.setAdapter(userAdapter);
        }
    }

    @Override
    public void onFailure(Call<List<User>> call, Throwable t) {

    }

    public User getUser(int position){
        return reverseUsers.get(position);
    }

    private ArrayList<User> getLimitItems(ArrayList<User> users){
        ArrayList<User> limitUsers = new ArrayList<>();
        int i = 0;
        if(mCount == 0)
        {
            for (User user:users) {
                if(i < 10){
                    limitUsers.add(user);
                    i++;
                }
            }
        }else {
            for (User user:users){
                if(i < mCount+10){
                    limitUsers.add(user);
                    i++;
                }
            }
        }
        return  reverseArray(limitUsers);
    }

    private ArrayList<User> reverseArray(ArrayList<User> array){
        reverseUsers = new ArrayList<>();
        for(int i = array.size()-1; i>=0; i--){
            reverseUsers.add(array.get(i));
        }
        return reverseUsers;
    }
}
