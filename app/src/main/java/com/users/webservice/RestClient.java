package com.users.webservice;

import retrofit2.GsonConverterFactory;
import retrofit2.Retrofit;


public class RestClient {

    private static final String BASE_URL = "https://api.github.com/";

    private static RestClient restClientInstance;
    private RestInterface restInterface;
    private Retrofit retrofit;

    public static RestClient getRestClientInstance() {
        if (restClientInstance == null) {
            restClientInstance = new RestClient();
        }
        return restClientInstance;
    }

    private RestClient() {
        setupRestClient();
    }

    private void setupRestClient(){
        retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
         restInterface = retrofit.create(RestInterface.class);
    }

    public RestInterface getService() {
        return restInterface;
    }

}