package com.users.webservice;

import com.users.models.User;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface RestInterface {
    @GET("users")
    Call<List<User>> getUsers();
}