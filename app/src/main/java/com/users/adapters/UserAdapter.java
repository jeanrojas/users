package com.users.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.users.R;
import com.users.models.User;

import java.util.ArrayList;

/**
 * Created by JEAN CARLO on 26/2/2016.
 */
public class UserAdapter extends ArrayAdapter<User>{
    
    // View lookup cache
    private static class ViewHolder {
        TextView textViewUser;
        ImageView imageView;
    }

    public UserAdapter(Context context, ArrayList<User> users) {
        super(context, R.layout.listitem_user, users);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        User user = getItem(position);
        ViewHolder viewHolder;
        if(convertView == null){
            viewHolder = new ViewHolder();
            LayoutInflater layoutInflater = LayoutInflater.from(getContext());
            convertView = layoutInflater.inflate(R.layout.listitem_user, parent, false);
            viewHolder.textViewUser = (TextView)convertView.findViewById(R.id.textViewUsername);
            viewHolder.imageView = (ImageView)convertView.findViewById(R.id.imageViewUser);
            convertView.setTag(viewHolder);
        }else{
            viewHolder = (ViewHolder)convertView.getTag();
        }
        viewHolder.textViewUser.setText(user.getLogin());
        Picasso.with(getContext()).load(user.getAvatarUrl()).into(viewHolder.imageView);
        return convertView;
    }
}